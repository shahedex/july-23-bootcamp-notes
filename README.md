# july-23-bootcamp-notes



## Getting started
This is the first *git* repo for **July 23 Bootcamp**

## To Do Tasks

- [x] Learn Git
- [] Sign up for GitLab
- [] Sign up for TS4U Git
- [] Clone the Repo
- [] Deploy Two-Tier Application

## How to push to git repository
```
git clone
git add .
git commit -m "commit message"
git push -u origin main

```
